package com.example.testplaceitbackend;


import android.os.Bundle;
import android.view.Menu;
import android.view.View;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

public class AddPlaceitActivity extends Activity {
	public static final String TAG = "AddItemActivity";
	private EditText item_name;
	private EditText item_desc;
	private EditText item_lat;
	private EditText item_long;
	
	ProgressDialog dialog;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_placeit);
		Button registerItem = (Button) findViewById(R.id.register_item);
		item_name = (EditText) findViewById(R.id.placeit_name);
		item_desc = (EditText) findViewById(R.id.placeit_desc);
		item_lat = (EditText) findViewById(R.id.placeit_lat);
		item_long = (EditText) findViewById(R.id.placeit_long);
		
		registerItem.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				postdata();
				Intent myIntent = new Intent(AddPlaceitActivity.this, MainActivity.class);
				startActivity(myIntent);
			}
		});
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.add_placeit, menu);
		return true;
}

	private void postdata() {
		final ProgressDialog dialog = ProgressDialog.show(this,
				"Posting Data...", "Please wait...", false);
		Thread t = new Thread() {

			public void run() {
				HttpClient client = new DefaultHttpClient();
				HttpPost post = new HttpPost(MainActivity.ITEM_URI);
 
			    try {
			      List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(7);
			      
			      nameValuePairs.add(new BasicNameValuePair("name",
			    		  item_name.getText().toString()));
			      nameValuePairs.add(new BasicNameValuePair("description",
			    		  item_desc.getText().toString()));
			      nameValuePairs.add(new BasicNameValuePair("latitude",
			    		  item_lat.getText().toString()));
			      nameValuePairs.add(new BasicNameValuePair("longitude",
			    		  item_long.getText().toString()));
			      nameValuePairs.add(new BasicNameValuePair("action",
				          "put"));
			      post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			  
			      HttpResponse response = client.execute(post);
			      BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			      String line = "";
			      while ((line = rd.readLine()) != null) {
			        Log.d(TAG, line);
			      }

			    } catch (IOException e) {
			    	Log.d(TAG, "IOException while trying to conect to GAE");
			    }
				dialog.dismiss();
			}
		};
		t.start();
		dialog.show();
	}
}
