package com.example.testplaceitbackend;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity {
	
	public static final String ITEM_URI = "http://cse110team25mycity.appspot.com/placeit";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Button addItem = (Button) findViewById(R.id.addPlaceit);
		Button showItems = (Button) findViewById(R.id.showPlaceit);
		
		addItem.setOnClickListener(new View.OnClickListener(){

			@Override
			public void onClick(View v) {
				Intent myIntent = new Intent(MainActivity.this, AddPlaceitActivity.class);
				startActivity(myIntent);
			}
		});
		
		showItems.setOnClickListener(new View.OnClickListener(){

			@Override
			public void onClick(View v) {
				Intent myIntent = new Intent(MainActivity.this, ShowPlaceitsActivity.class);
				startActivity(myIntent);
			}	
		});
	}
}