package com.example.testplaceitbackend;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class ShowPlaceitsActivity extends Activity {	
	
	public static final String TAG = "ShowItemsActivity";
	ProgressDialog dialog;
	private ListView placeit_names;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_show_placeits);
		placeit_names = (ListView) findViewById(R.id.show_placeits);
		// Now create a method which get query your Google App Engine via HTTP GET.
		// Make sure that method gets the data via a new thread
		// or use async task.
		String itemsData = getData();
		// Update the TextView with itemsData.
		
	}
	private String getData()
	{
		
		dialog = ProgressDialog.show(this, "Loading product data...", "Please wait...", false);
	    
		dialog.show();
		new UpdateItemsTask().execute(MainActivity.ITEM_URI);

		return ""; 
	}
	
	private class UpdateItemsTask extends AsyncTask<String, Void, List<String>> {
		 @Override
	     protected List<String> doInBackground(String... url) {
			 
			 	HttpClient client = new DefaultHttpClient();
				HttpGet request = new HttpGet(MainActivity.ITEM_URI);
				List<String> list = new ArrayList<String>();
				try {
					HttpResponse response = client.execute(request);
					HttpEntity entity = response.getEntity();
					String data = EntityUtils.toString(entity);
					Log.d(TAG, data);
					JSONObject myjson;
					
					try {
						myjson = new JSONObject(data);
						JSONArray array = myjson.getJSONArray("data");
						for (int i = 0; i < array.length(); i++) {
							JSONObject obj = array.getJSONObject(i);
							list.add(obj.get("name").toString());
						}
						
					} catch (JSONException e) {

				    	Log.d(TAG, "Error in parsing JSON");
					}
					
				} catch (ClientProtocolException e) {

			    	Log.d(TAG, "ClientProtocolException while trying to connect to GAE");
				} catch (IOException e) {

					Log.d(TAG, "IOException while trying to connect to GAE");
				}
	         return list;
	     }

	     protected void onPostExecute(List<String> list) {
	    	 ArrayAdapter<String> itemsAdapter = 
	    			    new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, list);
	    //	 ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getApplicationContext(),
	    //			 android.R.layout.simple_spinner_item, list);
	    //	 dataAdapter
		//	 		.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	    	 
	    	 placeit_names.setAdapter(itemsAdapter);
	    	 dialog.dismiss();
				
	     }

	 }
	
}